# Devgurus JS Interview Test

This project is a sample of a JS basic test when interviewing new potential candidates.

## Getting Started

To get started, just install the dependencies and run the tests.

For installing the dependencies, just run the following:

```
npm install
```

Once the dependencies are properly installed, you can run now the tests:

```
npm test
```
## Test Objectives
All unit tests in this project fail. Candidates needs to update the main code for all the unit tests to pass without updating "index.spec.js" in any way. Apart from that file, any other update will be consider valid.
The resulting updated project, should be zipped and sent back to Devgurus point of contact.