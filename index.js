const fetch = require('node-fetch');

// Given an array of numbers, return the sum of all of them
// i.e. given array [1, 2, 7]
// the function should return 1 + 2 + 7 => 10
export const sumValues = array => {
  // ***** TODO your code here *****
  return 0;
};

// Sorts an array of numbers
// sortDir: 0 -> Sort ascending (Make it the default one!)
// sortDir: 1 -> Sort descending
export const sortBy = (array, sortDir) => {
  // ***** TODO your code here *****
  return [];
};

// Returns true if the traditional dish is a Valencian one.
// ****** TODO There is something wrong here, fix it to make the test pass ******
export const isValencian = traditionalDish => {
  switch (traditionalDish) {
    case { name: 'Paella' }:
      return true;
    case { name: 'Fish&Chips' }:
      return false;
    case { name: 'Pizza' }:
      return false;
    default:
      return false;
  }
};

// Returns an array with the squared values
// i.e. given array = [1, 2, 3]
// the function should return a new array [1, 4, 9]
export const square = array => {
  // ***** TODO your code here *****
  return [];
};

// Returns a new array with the value concatenated to arr1
// i.e. given arr1 = [1, 5, 8] and value 9
// the function should return [1, 5, 8, 9]
// IMPORTANT NOTE: Do not use the Array.concat method
export const coolConcat = (arr1, value) => {
  // ***** TODO your code here *****
  return [];
};

// Given an array of strings, return a single string separating the strings with a space.
// IMPORTANT NOTE: do not use the `+` operator to concatenate the strings
// i.e: given ['a', 'b', 'c'] -> result 'a b c'
export const stringifyArray = array => {
  // ***** TODO your code here *****
  return '';
};

// Fix the test to make it work properly
export const resolveData = () =>
  new Promise((resolve, _) =>
    setTimeout(() => resolve('I have been resolved'), 2000)
  );

// Fix the test to make it work properly
export const getCatFacts = () =>
  fetch('https://cat-fact.herokuapp.com/facts')
    .then(res => res.json())
    .then(facts => facts.all.map(({ _id, text }) => ({ _id, text })));
