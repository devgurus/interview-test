import {
  sumValues,
  sortBy,
  isValencian,
  square,
  coolConcat,
  stringifyArray,
  resolveData,
  getCatFacts
} from './';

const array = [3, 6, 2, 4, 1, 8, 7, 9, 5];

describe('Interview test', () => {
  it('should return the sum of values', () => {
    expect(sumValues(array)).toBe(45);
  });

  it('should sort ascending', () => {
    expect(sortBy(array)).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9]);
  });

  it('should sort descending', () => {
    expect(sortBy(array, 1)).toEqual([9, 8, 7, 6, 5, 4, 3, 2, 1]);
  });

  it('should return true', () => {
    expect(isValencian({ name: 'Paella' })).toBeTruthy();
  });

  it('should return the square values', () => {
    expect(square([2, 4, 10])).toEqual([4, 16, 100]);
  });

  it('should return a new array concatenating the last value', () => {
    expect(coolConcat([1, 3, 4], 5)).toEqual([1, 3, 4, 5]);
  });

  // ***** TODO fix this test *****
  it('should return the resolved value', () => {
    expect(resolveData()).resolves.toBe('I have not been resolved');
  });

  // ***** TODO fix this test *****
  it('should resolve the cat facts array', () => {
    expect(getCatFacts()).resolves.toEqual([
      { _id: '1234', text: 'Some facts' },
      { _id: '4321', text: 'Some facts2' }
    ]);
  });
});
